Scrips, tools and more
======================

This is a repository for scripts and tools I have developed that are:

* Useful enough that they are worth keeping and potentially reusing
* Too small to justify creating a separate git repository for each of them
